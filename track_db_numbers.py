import pymongo
import urllib2
import sys
import json
import datetime
import unicodedata

_verbose_ = False
sqlSchema = "michel"
mongodbURL = 'mongodb://178.62.30.86:27017'

try:
	conn = pymongo.Connection('mongodb://bf:alls33ing1@178.62.30.86:27017')
except AttributeError:
	# New syntax for pymongo > 3.0.2
	conn = pymongo.MongoClient('mongodb://bf:alls33ing1@178.62.30.86:27017')		


f = open('./stats.txt', "a+")
date_updated = datetime.datetime.utcnow()
collNames = conn.database_names()
for name in collNames:
	connName = conn[name]
	print connName
	for cN in connName.collection_names():
		count = connName[cN].find({}).count()
		output = {'collection' : name+'.'+cN, 'date': date_updated.strftime("%Y%m%d"), 'count': count}
		f.write(json.dumps(output)+',')
f.close()
