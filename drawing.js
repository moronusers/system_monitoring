var plotAll = function(fName){
    getData(fName).then( function(res) {
        processData(res); 
    })   
    .fail( function (e){
        console.error(e);
    });

}

var getData = function(fname){
    return $.ajax({
    url: fname
    });

}
//Firefox hack for date.parse
function parseDateUTC(input) {
    var reg = /^(\d{4})-(\d{2})-(\d{2})$/; // (\d{2}):(\d{2}):(\d{2})$/;
    var parts = reg.exec(input);
    return parts ? (new Date(Date.UTC(parts[1], parts[2] -1, parts[3], parts[4], parts[5],parts[6]))) : null
}

var addChart = function(div, logdata, name){
    div.highcharts({
        chart: {
            type: 'spline',
            renderTo: div
        },
        title: {
            text: name //'Number of documents in collection.'
        },
        xAxis: {
            type: 'datetime',
            dateTimeLabelFormats: { // don't display the dummy year
                month: '%e. %b',
                year: '%b'
            },
            title: {
                text: 'Date'
            }
        },
        yAxis: {
            title: {
                text: 'Number of entries'
            },
            min: 0
        },
        tooltip: {
            headerFormat: '', //<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:.0f}'
        },

        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: [{
            name: name,
            data: logdata
        }]
    });// end highcharts

}

var processData = function (res) {
    var result = JSON.parse('['+res.substring(0,res.length-1)+']');
    var logdata = {};
    for (var i in result){
        if (logdata[result[i]['collection']] === undefined) {logdata[result[i]['collection']]=[];}
        var dd = result[i]['date'];
        var ddStr = dd.substr(0,4)+'-'+dd.substr(4,2)+'-'+dd.substr(6,2);
        var ddParsed = 
        logdata[result[i]['collection']].push([(new Date(ddStr)).getTime(), result[i]['count']]);
     }
     var container = $('#container');
     for (var k in logdata){
        if (!(k.includes('local.') || k.includes('admin.'))){
            var newDiv = document.createElement('div');
            addChart($(newDiv), logdata[k], k);
            container.append(newDiv);
        }
     }
};